package com.al.victime.model;

public enum Genre {
    AUTRE {
        @Override
        public String toString() {
            return "Autre";
        }
    }, FEMININ {
        @Override
        public String toString() {
            return "Madame";
        }
    }, MASCULIN {
        @Override
        public String toString() {
            return "Monsieur";
        }
    }
}
