package com.al.victime.repository;

import com.al.victime.model.Poste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.stereotype.Repository;

//@RepositoryRestResource
@Repository
@Table("poste")
public interface PosteRepository extends JpaRepository<Poste, Integer>, JpaSpecificationExecutor<Poste> {

    Poste findPosteById(Integer id);
}
