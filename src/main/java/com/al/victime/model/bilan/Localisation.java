package com.al.victime.model.bilan;

/**
 * Localisation des Traumatismes
 */
public enum Localisation {
    Tete, Crane, Oeil, Oreille, Cou, Poitrine, Ventre, Dos, Fesses, Bassin, Sexe, Epaule, AvBras, Bras, Coude, Poignet, Main, Cuisse, Jambe, Genou, Cheville, Pied
}
