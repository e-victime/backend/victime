package com.al.victime.model.bilan.Glasgow;

import java.util.HashMap;
import java.util.Map;

public enum Verbale implements Label {
    PR("Pas de réponse", 1),
    ICOMP("Incompréhensible (bruits, grognements)", 2),
    INAP("Inappropriée (propos incohérents)", 3),
    CONF("Confusion (désorientée)", 4),
    N("Normale", 5);

    private static final Map<String, Verbale> BY_LABEL = new HashMap<>();
    private static final Map<Integer, Verbale> BY_SCORE = new HashMap<>();

    static {
        for (Verbale v : values()) {
            BY_LABEL.put(v.label, v);
            BY_SCORE.put(v.score, v);
        }
    }

    public final String label;
    public final int score;

    Verbale(String label, int score) {
        this.label = label;
        this.score = score;
    }


    public static Verbale valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    public static Verbale valueOfAtomicNumber(int number) {
        return BY_SCORE.get(number);
    }

    @Override
    public String label() {
        return label;
    }

    @Override
    public Integer score() {
        return score;
    }


}
