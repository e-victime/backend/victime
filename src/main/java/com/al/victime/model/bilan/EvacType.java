package com.al.victime.model.bilan;

public enum EvacType {
    PROTEC, SP, SMUR, PRIVE, AUTRE, HELICO
}
