package com.al.victime.model.bilan.Glasgow;

public interface Label {
    String label();
    Integer score();
}
