package com.al.victime.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;
import java.io.Serializable;

@Embeddable
@Table(name = "members")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
public class MembersKey implements Serializable {

    @Column(name = "equipe_id")
    Integer equipeId;

    @Column(name = "personnel_id")
    Integer personnelId;
}
