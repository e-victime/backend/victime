package com.al.victime.model.bilan;

public enum TypeIntox {
    Alcool, CO, Medicament, Drogue, Alimentaire
}
