package com.al.victime.service;

import com.al.victime.model.Equipe;
import com.al.victime.model.Poste;
import com.al.victime.model.Victime;
import com.al.victime.model.bilan.Physique;
import com.al.victime.model.bilan.Psy;
import com.al.victime.model.bilan.Surveillance;
import com.al.victime.repository.*;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class VictimeServiceImpl implements VictimeService {

    // Repository

    @Autowired @Resource
    VictimeRepository victimeRepository;

    @Autowired @Resource
    PhysiqueRepository physiqueRepository;

    @Autowired @Resource
    PsyRepository psyRepository;

    @Autowired @Resource
    PosteRepository posteRepository;

    @Autowired @Resource
    EquipeRepository equipeRepository;

    @Autowired @Resource
    SurveillanceRepository surveillanceRepository;

    // Méthodes

    @Override
    public Optional<Victime> createVictime(Integer idPoste, Integer idEquipe, Victime victime) throws NotFoundException {
        Optional<Poste> poste = posteRepository.findById(idPoste);
        Optional<Equipe> equipe = equipeRepository.findById(idEquipe);

        if (poste.isPresent() && equipe.isPresent()) {
            Long v = victimeRepository.save(victime).getId();
            Set<Victime> listVictime = poste.get().getVictimes();

            return victimeRepository.findById(v).map(newVictime -> {
                newVictime.setNumeroVictime(new ArrayList<>(listVictime).indexOf(newVictime));
                newVictime.setBilanPhysique(new Physique(newVictime));
                newVictime.setBilanPsy(new Psy(newVictime));
                newVictime.setPoste(poste.get());
                newVictime.setEquipe(equipe.get());
                return victimeRepository.save(newVictime);
            });

        } else if (!poste.isPresent())
            throw new NotFoundException("Le poste avec l’id " + idPoste + " non trouvé.");
         else throw new NotFoundException("L’Equipe avec l’id " + idEquipe + " non trouvée.");
    }

    @Override
    public Optional<Surveillance> addSurveillance(Long idVictime, Surveillance surveillance) {
        Optional<Victime> victime = victimeRepository.findById(idVictime);
        if (victime.isPresent()) {
            Physique physique = victime.get().getBilanPhysique();
            surveillance.setBilan(physique);
            Integer idSurveillance = surveillanceRepository.save(surveillance).getId();
            return surveillanceRepository.findById(idSurveillance);
        } else
            return Optional.empty();
    }

    @Override
    public Optional<Victime> getVictime(Long idVictime) {
        return victimeRepository.findById(idVictime);
    }

    @Override
    public Page<Victime> getVictimesOnPoste(Integer idPoste, Pageable pageable) {
        Optional<Poste> poste = posteRepository.findById(idPoste);
        return poste.map(value -> victimeRepository.findVictimesByPoste(value, pageable)).orElse(Page.empty(pageable));
    }

    @Override
    public Page<Victime> getVictimesOnEquipe(Integer idPoste, Integer idEquipe, Pageable pageable) {
        Optional<Poste> poste = posteRepository.findById(idPoste);
        Optional<Equipe> equipe = equipeRepository.findById(idEquipe);
        if (poste.isPresent() && equipe.isPresent()) {
            return victimeRepository.findVictimesByPosteAndEquipe(poste.get(), equipe.get(), pageable);
        }
        return Page.empty(pageable);
    }

    @Override
    public Optional<Surveillance> getSurveillance(Integer idSurveillance) {
        return surveillanceRepository.findById(idSurveillance);
    }

    @Override
    public Optional<Physique> getBilanPhysique(Long idVictime) {
        Optional<Victime> victime = victimeRepository.findById(idVictime);
        return victime.isPresent() ? physiqueRepository.findPhysiqueByVictime(victime.get()) : Optional.empty();
    }

    @Override
    public Optional<Psy> getBilanPsy(Long idVictime) {
        Optional<Victime> victime = victimeRepository.findById(idVictime);
        return victime.isPresent() ? psyRepository.findPsyByVictime(victime.get()) : Optional.empty();
    }

    @Override
    public List<Victime> listVictimesOnPoste(Integer idPoste) {
        Optional<Poste> poste = posteRepository.findById(idPoste);
        return poste.map(value -> victimeRepository.getVictimesByPoste(value)).orElse(null);
    }

    @Override
    public Page<Surveillance> listSurveillances(Long idVictime, Pageable pageable) {
        Optional<Physique> bilan = physiqueRepository.findById(idVictime);
        return bilan.map(physique -> surveillanceRepository.findSurveillancesByBilan(physique, pageable)).orElse(null);
    }

    @Override
    public Optional<Victime> updateVictime(Victime victime, Long idVictime) {
        if (victimeRepository.findById(idVictime).isPresent()) {
            victime.setId(idVictime);
            Victime v = victimeRepository.save(victime);
            return victimeRepository.findById(v.getId());
        }
        return Optional.empty();
    }

    @Override
    public Optional<Physique> updateBilanPhysique(Physique bilan, Long idVictime) {
        Optional<Victime> victime = victimeRepository.findById(idVictime);
        if (victime.isPresent()) {
            Optional<Physique> physique = physiqueRepository.findPhysiqueByVictime(victime.get());
            bilan.setId(physique.get().getId());
            bilan.setVictime(victime.get());
            bilan.Calcul();
            if (!physique.get().getSurveillance().isEmpty())
                bilan.setSurveillance(physique.get().getSurveillance());
            bilan.setVictime(physique.get().getVictime());

            Physique p = physiqueRepository.save(bilan);
            return physiqueRepository.findById(p.getId());
        }
        return Optional.empty();
    }

    @Override
    public Optional<Psy> updateBilanPsy(Psy bilan, Long idVictime) {
        Optional<Victime> victime = victimeRepository.findById(idVictime);
        if (victime.isPresent()) {
            Optional<Psy> psy = psyRepository.findPsyByVictime(victime.get());
            bilan.setId(psy.get().getId());
            bilan.setVictime(victime.get());
            Psy p = psyRepository.save(bilan);
            return psyRepository.findById(p.getId());
        }
        return Optional.empty();
    }

    @Override
    public Optional<Surveillance> updateSurveillance(Surveillance surveillance, Integer idSurveillance) {
        Optional<Surveillance> oldSurveillance = surveillanceRepository.findById(idSurveillance);
        if (oldSurveillance.isPresent()) {
            surveillance.setId(idSurveillance);
            surveillance.setBilan(oldSurveillance.get().getBilan());
            Surveillance s = surveillanceRepository.save(surveillance);
            return surveillanceRepository.findById(s.getId());
        }
        return Optional.empty();
    }

    @Override
    public Boolean deleteVictime(Long idVictime) {
        Optional<Victime> victime = victimeRepository.findById(idVictime);
        if (victime.isPresent()) {
            surveillanceRepository.deleteAll(surveillanceRepository.getSurveillancesByBilan(victime.get().getBilanPhysique()));
            physiqueRepository.delete(victime.get().getBilanPhysique());
            psyRepository.delete(victime.get().getBilanPsy());
            victimeRepository.deleteById(idVictime);
        }
        return victimeRepository.findById(idVictime).isPresent();
    }

    @Override
    public Boolean deleteSurveillance(Integer idSurveillance) {
        surveillanceRepository.deleteById(idSurveillance);
        return surveillanceRepository.findById(idSurveillance).isPresent();
    }

    @Override
    public Boolean deleteSurveillances(Long idVictime) {
        Optional<Victime> victime = victimeRepository.findById(idVictime);
        if (victime.isPresent()) {
            surveillanceRepository.deleteAll(victime.get().getBilanPhysique().getSurveillance());
            return victime.get().getBilanPhysique().getSurveillance().isEmpty();
        } else
            return false;
    }

    @Override
    public Page<Victime> searchVictime(Specification<Victime> specs, Pageable pageable) {
        return victimeRepository.findAll(specs, pageable);
    }
}
