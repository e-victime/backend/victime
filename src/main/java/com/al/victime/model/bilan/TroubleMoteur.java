package com.al.victime.model.bilan;

/**
 * List des troubles moteurs
 */
public enum TroubleMoteur {
    /**
     * AUCUN : Aucun trouble moteur
     * MSD : Trouble membre Supérieur Droit
     * MSG : Trouble membre Supérieur Gauche
     * MID : Trouble membre Inférieur Droit
     * MIG : Trouble membre Inférieur Gauche
     * MS2 : Trouble 2 membres Supérieurs
     * MI2 : Trouble 2 membres Inférieurs
     * M4 : Trouble de tous les membres
     */
    AUCUN,MSD,MSG,MID,MIG,MS2,MI2,M4
}
