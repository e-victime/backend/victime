package com.al.victime.repository;

import com.al.victime.model.Victime;
import com.al.victime.model.bilan.Psy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Table("bilan_psy")
public interface PsyRepository extends JpaRepository<Psy,Long>, JpaSpecificationExecutor<Psy> {
    Optional<Psy> findPsyByVictime(Victime victime);
}
