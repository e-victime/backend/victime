package com.al.victime.controller;

import com.al.victime.model.Victime;
import com.al.victime.model.bilan.Physique;
import com.al.victime.model.bilan.Psy;
import com.al.victime.model.bilan.Surveillance;
import com.al.victime.service.VictimeService;
import com.sipios.springsearch.anotation.SearchSpec;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/victime")
public class VictimeController {

    @Autowired @Resource
    VictimeService victimeService;

    //Read

    @GetMapping(value = "/search")
    public ResponseEntity<Page<Victime>> searchVictime(@SearchSpec Specification<Victime> specs, Pageable pageable) {
        return new ResponseEntity<>(victimeService.searchVictime(specs, pageable), HttpStatus.OK);
    }

    @GetMapping("/poste/{idPoste}/equipe/{idEquipe}/victime")
    public ResponseEntity<Page<Victime>> getVictimesOnEquipe(@PathVariable("idPoste") Integer idPoste, @PathVariable("idEquipe") Integer idEquipe, Pageable pageable) {
        Page<Victime> victimes = victimeService.getVictimesOnEquipe(idPoste, idEquipe, pageable);
        HttpStatus status = victimes.isEmpty() ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return new ResponseEntity<>(victimes, status);
    }

    @GetMapping("/poste/{idPoste}/victime")
    public ResponseEntity<Page<Victime>> getVictimesOnPoste(@PathVariable("idPoste") Integer idPoste, Pageable pageable) {
        Page<Victime> victimes = victimeService.getVictimesOnPoste(idPoste, pageable);
        HttpStatus status = victimes.isEmpty() ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return new ResponseEntity<>(victimes, status);
    }

    @GetMapping("/poste/{idPoste}/all")
    public ResponseEntity<List<Victime>> ListVictimesOnPoste(@PathVariable("idPoste") Integer idPoste) {
        List<Victime> victimes = victimeService.listVictimesOnPoste(idPoste);
        HttpStatus status = victimes.isEmpty() ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return new ResponseEntity<>(victimes, status);
    }

    @GetMapping("/{idVictime}/surveillance")
    public ResponseEntity<Page<Surveillance>> getSurveillances(@PathVariable("idVictime") Long idVictime, Pageable pageable) {
        Page<Surveillance> surveillances = victimeService.listSurveillances(idVictime, pageable);
        HttpStatus status = surveillances.isEmpty() ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return new ResponseEntity<>(surveillances, status);
    }

    @GetMapping("/{idVictime}")
    public ResponseEntity<Victime> getVictime(@PathVariable Long idVictime) {
        Optional<Victime> victimeOptional = victimeService.getVictime(idVictime);
        HttpStatus status = victimeOptional.isPresent() ? HttpStatus.FOUND : HttpStatus.NOT_FOUND;
        Victime victime = victimeOptional.orElse(null);
        return new ResponseEntity<>(victime,status);
    }

    @GetMapping("/{idVictime}/physique")
    public ResponseEntity<Optional<Physique>> getPhysique(@PathVariable Long idVictime) {
        Optional<Physique> physique = victimeService.getBilanPhysique(idVictime);
        HttpStatus status = physique.isPresent() ? HttpStatus.FOUND : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(physique, status);
    }

    @GetMapping("/{idVictime}/psy")
    public ResponseEntity<Optional<Psy>> getPsy(@PathVariable Long idVictime) {
        Optional<Psy> psy = victimeService.getBilanPsy(idVictime);
        HttpStatus status = psy.isPresent() ? HttpStatus.FOUND : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(psy, status);
    }

    @GetMapping("/{idVictime}/surveillance/{idSurveillance}")
    public ResponseEntity<Optional<Surveillance>> getSurveillance(@PathVariable("idSurveillance") Integer idSurveillance) {
        Optional<Surveillance> surveillance = victimeService.getSurveillance(idSurveillance);
        HttpStatus status = surveillance.isPresent() ? HttpStatus.FOUND : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(surveillance, status);
    }

    // Create

    @PostMapping("/poste/{idPoste}/equipe/{idEquipe}/victime")
    public ResponseEntity<Optional<Victime>> createVictime(@PathVariable("idPoste") Integer idPoste, @PathVariable("idEquipe") Integer idEquipe, @RequestBody Victime victime) throws NotFoundException {
        return new ResponseEntity<>(victimeService.createVictime(idPoste, idEquipe, victime), HttpStatus.CREATED);
    }

    @PostMapping("/{idVictime}/surveillance")
    public ResponseEntity<Optional<Surveillance>> addSurveillance(@PathVariable("idVictime") Long idVictime, @RequestBody Surveillance surveillance) {
        Optional<Surveillance> s = victimeService.addSurveillance(idVictime,surveillance);
        HttpStatus status = s.isPresent() ? HttpStatus.CREATED : HttpStatus.INTERNAL_SERVER_ERROR;
        return new ResponseEntity<>(s, status);
    }

    // Update

    @PutMapping(value = "/{idVictime}")
    public ResponseEntity<Optional<Victime>> updateVictime(@PathVariable(value = "idVictime") Long idVictime, @Validated Victime victime) {
        Optional<Victime> updatedVictime = victimeService.updateVictime(victime,idVictime);
        HttpStatus status = updatedVictime.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(updatedVictime,status);
    }

    @PutMapping(value = "/{idVictime}/physique")
    public ResponseEntity<Optional<Physique>> updateBilan(@PathVariable(value = "idVictime") Long idVictime, @RequestBody Physique bilan) {
        Optional<Physique> updatedBilan = victimeService.updateBilanPhysique(bilan, idVictime);
        HttpStatus status = updatedBilan.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(updatedBilan,status);
    }

    @PutMapping(value = "/{idVictime}/psy")
    public ResponseEntity<Optional<Psy>> updateBilanPsy(@PathVariable(value = "idVictime") Long idVictime, @RequestBody Psy bilan) {
        Optional<Psy> updatedBilan = victimeService.updateBilanPsy(bilan, idVictime);
        HttpStatus status = updatedBilan.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(updatedBilan,status);
    }
    
    @PutMapping(value = "/{idVictime}/surveillance/{idSurveillance}")
    public ResponseEntity<Optional<Surveillance>> updateSurveillance(@PathVariable(value = "idSurveillance") Integer idSurveillance, @RequestBody Surveillance surveillance) {
        Optional<Surveillance> updatedSurveillance = victimeService.updateSurveillance(surveillance, idSurveillance);
        HttpStatus status = updatedSurveillance.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(updatedSurveillance,status);
    }

    // Delete

    @DeleteMapping(value = "/{idVictime}")
    public ResponseEntity<HttpStatus> deleteVictime(@PathVariable(value = "idVictime") Long idVictime) {
        HttpStatus status = victimeService.deleteVictime(idVictime) ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK;
        return new ResponseEntity<>(status);
    }

    @DeleteMapping(value = "/{idVictime}/surveillance/{idSurveillance}")
    public ResponseEntity<HttpStatus> deleteSurveillance(@PathVariable(value = "idSurveillance") Integer idSurveillance) {
        HttpStatus status = victimeService.deleteSurveillance(idSurveillance) ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK;
        return new ResponseEntity<>(status);
    }

    @DeleteMapping(value = "/{idVictime}/surveillances")
    public ResponseEntity<HttpStatus> deleteAllSurveillancesOnVictime(@PathVariable(value = "idVictime") Long idVictime) {
        HttpStatus status = victimeService.deleteSurveillances(idVictime) ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR;
        return new ResponseEntity<>(status);
    }

}
