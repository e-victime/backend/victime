package com.al.victime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class VictimeApplication {

    public static void main(String[] args) {
        SpringApplication.run(VictimeApplication.class, args);
    }

}
