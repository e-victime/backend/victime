package com.al.victime.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "members")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@ToString(of = {
        "role"
})
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode
public class Members {

    @EmbeddedId
    @NonNull
    MembersKey id;

    @NonNull
    @ManyToOne
    @MapsId("equipeId")
    @JoinColumn(name = "equipe_id")
    @JsonBackReference(value = "PersonnelsForEquipe")
    Equipe equipe;

    @NonNull
    @ManyToOne
    @MapsId("personnelId")
    @JoinColumn(name = "personnel_id")
    @JsonBackReference(value = "EquipesForPersonnel")
    Personnel personnel;

    @Column(name = "role")
    String role;
}
